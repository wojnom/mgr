from src.infrastructure.config import LoggerConfig
from src.view.View import window

if __name__ == '__main__':
    LoggerConfig.logger_config()
    window()
