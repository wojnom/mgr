from src.infrastructure.DisplayedImageContext import DisplayedImageContext


class DisplayedImageContextHolder:
    displayed_image_context: DisplayedImageContext = None
