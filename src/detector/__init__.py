import cv2

from src.detector.DetectedFacesHighlighter import DetectedFacesHighlighter
from src.detector.YoloDetectionOutputFacesExtractor import YoloDetectionOutputFacesExtractor
from src.detector.YoloDetector import YoloDetector
from src.infrastructure.DisplayedImageContextHolder import DisplayedImageContextHolder

detector = YoloDetector()
faces_extractor = YoloDetectionOutputFacesExtractor()
highlighter = DetectedFacesHighlighter()


def detect_faces_and_highlight():
    displayed_image_context = DisplayedImageContextHolder.displayed_image_context
    # shortcut, obviously cv img should be constructed from byte buffer
    image = cv2.imread(displayed_image_context.location)
    grey_img = get_gray_scale_img(image)

    outs = detector.detect(grey_img)
    detections = faces_extractor.extract(outs, displayed_image_context.width, displayed_image_context.height)
    image_with_highlighted_result = highlighter.highlight(detections, image)
    result_path = get_result_img_location(displayed_image_context.location)
    cv2.imwrite(result_path, image_with_highlighted_result)
    return image_with_highlighted_result


def get_gray_scale_img(image):
    grey_scale_one_channel = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    return cv2.merge((grey_scale_one_channel, grey_scale_one_channel, grey_scale_one_channel))


def get_result_img_location(location):
    split_path = location.split('/')
    fileName = split_path[len(split_path) - 1]
    split_path[len(split_path) - 1] = "resultInverted"
    split_path.append(fileName)
    return '/'.join(split_path)
