from src.detector.DetectedFaces import DetectedFaces
import cv2


class DetectedFacesHighlighter:

    def highlight(self, detected_faces: DetectedFaces, img):
        for i in range(len(detected_faces.boxes)):
            if i in detected_faces.indexes:
                x, y, w, h = detected_faces.boxes[i]
                confidence_label = "Confidence: " + str(detected_faces.confidences[i])
                color = detected_faces.colors[detected_faces.class_ids[i]]
                cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
                cv2.putText(img, confidence_label, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 1 / 2, color, 2)
        return img
