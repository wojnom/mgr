import numpy as np
import cv2

from src.detector.DetectedFaces import DetectedFaces


class YoloDetectionOutputFacesExtractor:

    def __init__(self):
        self.object_detection_threshold = 0.5
        self.classes = ["face"]

    def extract(self, yolo_detection_outputs, image_width, image_height):
        class_ids = []
        confidences = []
        boxes = []
        indexes = []
        colors = []

        for out in yolo_detection_outputs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > self.object_detection_threshold:
                    # Object detected
                    center_x = int(detection[0] * image_width)
                    center_y = int(detection[1] * image_height)
                    w = int(detection[2] * image_width)
                    h = int(detection[3] * image_height)
                    # Rectangle coordinates
                    x = int(center_x - w / 2)
                    y = int(center_y - h / 2)
                    boxes.append([x, y, w, h])
                    confidences.append(float(confidence))
                    class_ids.append(class_id)
                    # We use NMS function in opencv to perform Non-maximum Suppression
                    # #we give it score threshold and nms threshold as arguments.
                    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
                    colors = np.random.uniform(0, 255, size=(len(self.classes), 3))

        return DetectedFaces(indexes, colors, self.classes, class_ids, boxes, confidences)
