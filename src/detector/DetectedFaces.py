class DetectedFaces:

    def __init__(self, indexes, colors, classes, class_ids, boxes, confidences):
        self.indexes = indexes
        self.colors = colors
        self.classes = classes
        self.class_ids = class_ids
        self.boxes = boxes
        self.confidences = confidences
