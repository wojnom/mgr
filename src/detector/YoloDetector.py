import cv2


class YoloDetector:

    def __init__(self):
        self.net = cv2.dnn.readNetFromDarknet("resources/thermal.cfg", "resources/thermal_final.weights")
        self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
        self.layer_names = self.net.getLayerNames()
        self.output_layers = [self.layer_names[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]

    def detect(self, img):
        img = cv2.resize(img, None, fx=0.4, fy=0.4)
        blob = cv2.dnn.blobFromImage(img, 1 / 255.0, (416, 416), swapRB=True, crop=False)
        self.net.setInput(blob)
        return self.net.forward(self.output_layers)
