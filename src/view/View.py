import sys

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QAction, QMainWindow, QLabel

from src.view import ViewController


def window():
    app = QApplication(sys.argv)
    main_window = QMainWindow(None)
    label = QLabel(main_window)
    main_window.setGeometry(50, 50, 620, 800)

    menubar = main_window.menuBar()

    file_menu = menubar.addMenu("&File")
    load_file_action = QAction("Load file")
    load_file_action.triggered \
        .connect(lambda: ViewController.load_image(main_window, label))
    file_menu.addAction(load_file_action)

    image_menu = menubar.addMenu("&Image")
    detect_faces_action = QAction("Detect faces")
    detect_faces_action.triggered \
        .connect(lambda: ViewController.detect_faces(main_window, label))
    image_menu.addAction(detect_faces_action)

    main_window.show()
    label.show()

    sys.exit(app.exec_())
