import io

from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtWidgets import QFileDialog

from src.infrastructure.DisplayedImageContextHolder import DisplayedImageContextHolder
from src.infrastructure.DisplayedImageContext import DisplayedImageContext
import src.detector as detector


def load_image(main_window, label):
    name = QFileDialog.getOpenFileName(main_window, 'Open file', 'c:\\', "Image files (*.png *.jpg *.bmp)")
    pixmap = QPixmap()
    with io.open(name[0], 'rb') as fp:
        image_buffer = fp.read()
        pixmap.loadFromData(image_buffer)
        image_width = pixmap.width()
        image_height = pixmap.height()
        DisplayedImageContextHolder.displayed_image_context = \
            DisplayedImageContext(image_buffer, image_width, image_height, name[0])
        fp.close()

    label.setPixmap(pixmap)
    label.resize(pixmap.width(), pixmap.height())
    main_window.resize(pixmap.width(), pixmap.height())


def detect_faces(main_window, label):
    image_data = detector.detect_faces_and_highlight().data
    displayed_image_context = DisplayedImageContextHolder.displayed_image_context
    bytes_per_line = displayed_image_context.width * 3
    q_image = QImage(image_data, displayed_image_context.width, displayed_image_context.height, bytes_per_line, QImage.Format_RGB888).rgbSwapped()
    label.setPixmap(QPixmap(q_image))
